#-------------------------------------------------
#
# Project created by QtCreator 2016-11-02T14:04:24
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = mysql
TEMPLATE = app
QT += sql

SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
