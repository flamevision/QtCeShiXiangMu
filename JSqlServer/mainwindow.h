#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QMessageBox>
#include <QDebug>
#include <QtSql>
//#include <QtSql/QSql>
//#include <QtSql/QSqlDatabase>
//#include <QtSql/QSqlQueryModel>
//#include <QtSql/QSqlError>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;


    bool OpenDatabase();
};

#endif // MAINWINDOW_H
