﻿#include "thread1.h"
#include <QDebug>
#include <windows.h>

extern DataBaseInfo DbInfo;

Thread1::Thread1(QObject *parent) : QObject(parent)
{

}

void Thread1::DoWork()
{
    QVector<AreaInfo> List;
    JMySQL *mysql = new JMySQL(DbInfo, QString::number(DWORD(QThread::currentThreadId())));
    mysql->ReadAreaInfo(List);
    delete mysql;
    for(auto i = 0; i < List.size(); i++)
        qDebug()<< QThread::currentThreadId() << ": " << List.at(i).AreaName;
}
