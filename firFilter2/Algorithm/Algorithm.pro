#-------------------------------------------------
#
# Project created by QtCreator 2020-11-20T09:29:02
#
#-------------------------------------------------

QT       -= gui

TARGET = Algorithm
TEMPLATE = lib
CONFIG(debug, debug | release) {
    DESTDIR  += $$PWD/../lib_debug
} else {
    DESTDIR  += $$PWD/../lib_release
}

DEFINES += ALGORITHM_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        algorithm.cpp \
    complex.cpp

HEADERS += \
        algorithm.h \
        algorithm_global.h \ 
    complex.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
