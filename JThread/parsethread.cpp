﻿#include "parsethread.h"
#include <QDebug>
#include <QDateTime>

ParseThread::ParseThread(QObject *parent) : QObject(parent)
{

}

void ParseThread::doWork()
{
    qDebug() << "Parse thread: " << QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz");
    QThread::sleep(2);
}

void ParseThread::Stop()
{
    qDebug() << "Stop parse hread: " << QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz");
}
